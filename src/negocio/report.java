/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.Infopersonas;
import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class report {

    Infopersonas infper = new Infopersonas();
    public String [] nombrecol={"NOMBRE","CEDULA","GENERO","TOTAL","ASIENTOS"};
    public String[][] reporte() {
        ArrayList listarchivo = infper.LeerDesdeArchivo();
        String[][] repo = new String[listarchivo.size()][5];
        for (int i = 0; i < listarchivo.size(); i++) {
            String nombre = "", cedula = "", genero = "", total = "", asientos = "";
            String nomb = (String) listarchivo.get(i);
            String nom[] = nomb.split("/");
            nombre = nom[0];
            cedula = nom[1];
            genero = nom[2];
            if (nom.length>=5) {
                total = nom[3];
                asientos = nom[4];
            }
            repo[i][0] = nombre;
            repo[i][1] = cedula;
            repo[i][2] = genero;
            repo[i][3] = total;
            repo[i][4] = asientos;
        }
        return repo;
    }
}
